﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using MiniJSON;
using System;
using System.Collections.Generic;
using GeoCoordinatePortable;
public class Presenter : MonoBehaviour {
	[SerializeField] View view;
    [SerializeField] LocationUpdater updater;
	[SerializeField] HereMapApi hereMapApi;
	[SerializeField] HereGeocoderApi hereGeocoderApi;
	[SerializeField] SwaggerApi swaggerApi;
	[SerializeField] HereRoutingApi hereRoutingApi;
	[SerializeField] Material redMat;
	[SerializeField] Material greenMat;
	void Start()
	{
		StartCoroutine(Initialize());
	}
    IEnumerator Initialize()
    {
		view.ButtonArrow.onClick.AddListener(OnClickButtonArrow);
		view.InputField.onEndEdit.AddListener(delegate(string text) {
			ValidateText(text);
        });
        while(!updater.CanGetLonLat())
            yield return null;
		swaggerApi.Call(updater.Latitude.ToString(), updater.Longitude.ToString(), "false", "1", (res) =>
		{
			IDictionary jsonData = (IDictionary)Json.Deserialize(res);
			if(jsonData == null) return;
			IDictionary _jsonData = (IDictionary)jsonData["results"];
			IList tweets = (IList)_jsonData["tweets"];
			foreach(IDictionary tweet in tweets)
			{
				float lat = Convert.ToSingle(tweet["full_lat"]);
				float lng = Convert.ToSingle(tweet["full_lng"]);
				Transform objSentiment = Instantiate(view.PrefabTwitter);
				objSentiment.SetParent(view.TransformTwitter);
				objSentiment.localPosition = Vector3.zero;
				var _distance = new GeoCoordinate(updater.Latitude, updater.Longitude).GetDistanceTo(new GeoCoordinate(lat, lng));
				Location _current = new Location(updater.Latitude, updater.Longitude);
				Location _destination = new Location(lat, lng);
				objSentiment.localRotation = Quaternion.Euler(0, 0, -(float)NaviMath.LatlngDirection(_current, _destination));
				Transform t = objSentiment.Find("SentimentBar");
				t.localPosition = new Vector3(0, (float)_distance, 0);
       			ParticleSystem.MainModule par = t.GetComponent<ParticleSystem>().main;
				ParticleManager particleManager = t.GetComponent<ParticleManager>();
				particleManager.speed = Convert.ToSingle(tweet["time"]);
				if(Convert.ToSingle(tweet["sentiment"]) > 0.5f)
				{
					par.startColor = Color.green;
					t.GetComponent<TrailRenderer>().material = greenMat;
				}
				else
				{
					par.startColor = Color.red;
					t.GetComponent<TrailRenderer>().material = redMat;
				}
			}		
		});
		hereMapApi.Call(updater.Latitude.ToString(), updater.Longitude.ToString(), (res) =>
        {
            if(res != null)
                view.RawImage.texture = res;
		});
	}
	void OnClickButtonArrow()
	{
		view.AnimatorHeader.SetTrigger("OnClick");
	}
	void ValidateText(string text)
	{
		Debug.Log(text);
		if(text.ToLower() == "taipei")
		{
			text = "https://sky-gazer3.glitch.me/";
			if(RegexUtils.IsUrl(text))
				StartCoroutine(LoadWebView(text));
		}
		else
		{
			GetDestinationPoint(text);
		}
	}
	IEnumerator LoadWebView(string url)
	{
		view.AnimatorHeader.SetTrigger("OnLoad");
		yield return new WaitForSeconds(1.5f);
		view.WebView.enabled = true;
		view.WebView.SetUrl(url);
		StartCoroutine(view.WebView.Load());
	}
	public void GetDestinationPoint(string str)
	{
		hereGeocoderApi.Call(str, (res) =>
        {
			if(res == "-1")
			{
				Debug.Log("Network error");
			}
			else if(res == "-2")
			{
				Debug.Log("Network error");
			}
			else
			{
				IDictionary jsonData = (IDictionary)Json.Deserialize(res);
				if(jsonData == null) return;
				IDictionary response = (IDictionary)jsonData["Response"];
				IList view = (IList)response["View"];
				foreach(IDictionary _view in view){
					IList result = (IList)_view["Result"];
					foreach(IDictionary _result in result){
						IDictionary location = (IDictionary)_result["Location"];
						IDictionary displayPosition = (IDictionary)location["DisplayPosition"];
						float lat = Convert.ToSingle(displayPosition["Latitude"]);
						float lon = Convert.ToSingle(displayPosition["Longitude"]);
						CallRoutingApi(lat.ToString(), lon.ToString());
					}
				}
			}
		});	
	}
	void CallRoutingApi(string dest_lat, string dest_lon)
	{
		view.PrefabDestination.Find("SentimentBar").localPosition = new Vector3(0, 0, 100);
		view.PrefabDestination.gameObject.SetActive(false);
		hereRoutingApi.Call(updater.Latitude.ToString(), updater.Longitude.ToString(), dest_lat, dest_lon, (res) =>
		{
			IDictionary jsonData = (IDictionary)Json.Deserialize(res);
			if(jsonData == null) return;
			double distance;
			Location current;
			Location destination;
			if(!string.IsNullOrEmpty(jsonData["type"].ToString()))
			{
				// Success
				distance = new GeoCoordinate(updater.Latitude, updater.Longitude).GetDistanceTo(new GeoCoordinate(Convert.ToSingle(dest_lat), Convert.ToSingle(dest_lon)));
				current = new Location(updater.Latitude, updater.Longitude);
				destination = new Location(Convert.ToSingle(dest_lat), Convert.ToSingle(dest_lon));			
			}
			else
			{
				// Failure
				distance = new GeoCoordinate(updater.Latitude, updater.Longitude).GetDistanceTo(new GeoCoordinate(Convert.ToSingle(dest_lat), Convert.ToSingle(dest_lon)));
				current = new Location(updater.Latitude, updater.Longitude);
				destination = new Location(Convert.ToSingle(dest_lat), Convert.ToSingle(dest_lon));
			}
			StartCoroutine(SetDestination(-(float)NaviMath.LatlngDirection(current, destination), (float)distance));
		});					
	}
	IEnumerator SetDestination(float rot, float pos)
	{
		yield return null;
		view.PrefabDestination.gameObject.SetActive(true);
		view.PrefabDestination.localRotation = Quaternion.Euler(0, 0, rot);
		view.PrefabDestination.Find("SentimentBar").localPosition = new Vector3(0, pos, 100);
	}
}