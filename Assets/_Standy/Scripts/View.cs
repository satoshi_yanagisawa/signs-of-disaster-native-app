﻿using UnityEngine;
using UnityEngine.UI;
public class View : MonoBehaviour
{
	[SerializeField] Button buttonArrow;
	public Button ButtonArrow
	{
		get { return buttonArrow; }
	}
	[SerializeField] Animator animatorHeader;
	public Animator AnimatorHeader
	{
		get { return animatorHeader; }
	}
	[SerializeField] InputField inputField;
	public InputField InputField
	{
		get { return inputField; }
	}
	[SerializeField] WebView webView;
	public WebView WebView
	{
		get { return webView; }
	}
    [SerializeField] RawImage rawImage;
	public RawImage RawImage
	{
		get { return rawImage; }
	}
	[SerializeField] Transform transformTwitter;
	public Transform TransformTwitter
	{
		get { return transformTwitter; }
	}
	[SerializeField] Transform prefabTwitter;
	public Transform PrefabTwitter
	{
		get { return prefabTwitter; }
	}
	[SerializeField] Transform prefabDestination;
	public Transform PrefabDestination
	{
		get { return prefabDestination; }
	}
	[SerializeField] Transform prefabWind;
	public Transform PrefabWind
	{
		get { return prefabWind; }
	}
}
