﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class ParticleManager : MonoBehaviour {
	public float speed;
	void Update () {
       	transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, Mathf.PingPong(Time.time, 1) * 1000 * speed * Random.Range(0.1f, 1.9f));
	}
}