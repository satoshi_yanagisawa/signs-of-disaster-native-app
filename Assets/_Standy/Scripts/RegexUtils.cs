﻿using System.Text.RegularExpressions;
/// <summary>
/// Regex クラスに関する汎用関数を管理するクラス
/// </summary>
public static class RegexUtils
{
    /// <summary>
    /// 指定された文字列が URL かどうかを返します
    /// </summary>
    public static bool IsUrl( string input )
    {
        if ( string.IsNullOrEmpty( input ) )
        {
            return false;
        }
        return Regex.IsMatch( 
           input, 
           @"^s?https?://[-_.!~*'()a-zA-Z0-9;/?:@&=+$,%#]+$" 
        );
    }
}